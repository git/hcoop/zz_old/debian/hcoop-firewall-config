# -*- shell-script -*-
#
#  Configuration file for ferm(1).
#

table filter {
    chain INPUT {
        policy DROP;

        # connection tracking
        mod state state INVALID DROP;
        mod state state (ESTABLISHED RELATED) ACCEPT;

        # allow local packet
        interface lo ACCEPT;

        # respond to ping
        proto icmp ACCEPT; 

        # allow IPsec
        proto udp dport 500 ACCEPT;
        proto (esp ah) ACCEPT;

	proto tcp dport ssh ACCEPT;

        # domtool slave
	proto tcp dport 1235 ACCEPT;

	proto (tcp udp) dport ( kerberos afs3-callback ) ACCEPT;

	# system ports
	@include 'service.in.d/';
	@include 'local_ports_in.conf';
	@include 'users_tcp_in.conf';
    }
    chain OUTPUT {
        policy DROP;

        # connection tracking
        #mod state state INVALID DROP;
        mod state state (ESTABLISHED RELATED) ACCEPT;

	# Allow ping
	proto icmp ACCEPT;

	proto tcp dport ssh ACCEPT;

        # connections to domtool dispatcher and slaves (for e.g. QFoo)
	proto tcp dport 1234 ACCEPT;
	proto tcp dport 1235 ACCEPT;

	proto (tcp udp) dport ( kerberos afs3-fileserver afs3-callback afs3-prserver afs3-vlserver afs3-volser afs3-errors afs3-bos ) ACCEPT;
	proto tcp dport kerberos-adm ACCEPT;

	proto (tcp udp) dport ntp ACCEPT;
	proto (tcp udp) dport domain ACCEPT;

	# Allow mail to be routed to smarthost. This may be less than
	# idea: it seems safer to generate a list of system users to
	# allow, and then require individual members to request smtp
	# access. We'll leave that for the wheezy machines.
	proto tcp dport smtp daddr mail.hcoop.net ACCEPT;

	# At least for now, open ports to database servers. If dbtool
	# adduser could also add firewall rules, that would be better.
	proto (tcp udp) dport mysql daddr mysql.hcoop.net ACCEPT;
	proto (tcp udp) dport (postgresql 5433) daddr postgres.hcoop.net ACCEPT;

        # root needs port 80 for things like apt-get
	mod owner uid-owner 0 { proto (tcp) dport (http https) ACCEPT; }

	@include 'service.out.d/';
	@include 'local_ports_out.conf';
	@include 'users_tcp_out.conf';
    }
    chain FORWARD {
        policy DROP;

        # connection tracking
        mod state state INVALID DROP;
        mod state state (ESTABLISHED RELATED) ACCEPT;
    }
}

@include 'user_chains.conf';

# IPv6:
#domain ip6 {
#    table filter {
#        chain INPUT {
#            policy ACCEPT;
#            # ...
#        }
#        # ...
#    }
#}
